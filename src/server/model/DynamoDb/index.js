import AWS from 'aws-sdk';

AWS.config.update({
  region: 'us-east-1',
  endpoint: 'dynamodb.us-east-1.amazonaws.com'
});

function insertRawUplink(uplink) {
  const { dev_id, metadata, payload_fields } = uplink;
  const { frequency, data_rate } = metadata;
  const { temperature_1, relative_humidity_2 } = payload_fields;

  const Item = {
    dev_id,
    time: new Date().getTime(),
    data: {
      temperature: temperature_1,
      humidity: relative_humidity_2
    },
    rf_data: {
      frequency,
      data_rate
    }
  };

  const docClient = new AWS.DynamoDB.DocumentClient();
  const params = {
    TableName: 'rtu-data',
    Item
  };

  return docClient.put(params).promise();
}

async function queryUplinks(devices, start, end) {
  const docClient = new AWS.DynamoDB.DocumentClient();
  const requests = devices.map(device => {
    const params = {
      TableName: 'rtu-data',
      KeyConditionExpression: 'dev_id = :d and #t between :s and :e',
      ExpressionAttributeNames:{
        "#t": "time"
      },
      ExpressionAttributeValues: {
        ':d': device,
        ':s': start,
        ':e': end
      }
    };

    return docClient.query(params).promise();
  });

  const uplinks = await Promise.all(requests);
  return devices.reduce((accum, device, i) => {
    accum[device] = uplinks[i].Items;
    return accum;
  }, {});
}

async function getLatest(devices) {
    const docClient = new AWS.DynamoDB.DocumentClient();
    const requests = devices.map(device => {
      const params = {
        TableName: 'rtu-data',
        KeyConditionExpression: 'dev_id = :d',
        Limit: 1,
        ScanIndexForward: false,
        ExpressionAttributeValues: {
          ':d': device
        }
      };

      return docClient.query(params).promise();
    });

    const uplinks = await Promise.all(requests);
    return devices.reduce((accum, device, i) => {
      accum[device] = uplinks[i].Items[0].time;
      return accum;
    }, {});
}

export default {
  insertRawUplink,
  queryUplinks,
  getLatest
};
