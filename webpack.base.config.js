const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const path = require('path');

const config = {
  entry: path.resolve(__dirname, 'src/client/app/index.js'),
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.(sass|css)$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
          }
        ]
      }
    ]
  },
  resolve: {
    alias: {
      lib: path.resolve(__dirname, 'src/client/lib/'),
      styles: path.resolve(__dirname, 'src/client/app/styles/'),
    }
  },
  plugins: [
    new HtmlWebpackPlugin({ title: 'Holden Arboretum' }),
    new webpack.EnvironmentPlugin(['NODE_ENV'])
  ]
};

module.exports = config;
