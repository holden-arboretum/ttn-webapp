import { Router } from 'express';

export default () => {
  const api = Router();

  api.get('/oauth/callback', (req, res) => {
    const { error, error_description, error_uri, code } = req.query;
    if (error) {
      res.internalServerError({ error, error_description, error_uri });
    }
    res.cookie('authorization_code', code);
    res.redirect('http://34.232.30.167');
  });

  api.get('/oauth/token', (req, res) => {
    const { error, error_description, error_uri, token } = req.query;
    if (error) {
      res.internalServerError({ error, error_description, error_uri });
    }
    res.cookie('token', token);
    res.redirect('http://34.232.30.167');
  })

  return api;
}
