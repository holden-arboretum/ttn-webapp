import React from 'react';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import Divider from '@material-ui/core/Divider'
import Typography from '@material-ui/core/Typography';
import MicroIcon from '../../app/resources/micro.svg';
import Collapse from '@material-ui/core/Collapse';

import classNames from 'classnames'
import { fixHexString } from 'lib/strings';
import moment from 'moment';

import 'styles/device-tile.sass';

class DeviceTile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hovering: false
    };
  }

  render() {
    const { device } = this.props;
    const id = device.devId;
    const addr = fixHexString(device.devAddr);
    const daysSince = moment().diff(moment(device.last_seen), 'days');

    return (
      <Paper
        className="device"
        key={id}
        elevation={this.state.hovering ? 6 : 2}
        onMouseEnter={event => this.setState({ hovering: true })}
        onMouseLeave={event => this.setState({ hovering: false })}
        onMouseUp={() => this.props.onClick()}
      >
        <div className={daysSince <= 2 ? "device-status-online" : "device-status-offline" }/>
        <MicroIcon
          className={classNames("micro-icon", {
            "micro-icon-hovered": this.state.hovering
          })}
        />
        <Divider/>
        <div className="device-footer">
          <div className="device-footer-header">
            <Typography  variant="h6" color="inherit" noWrap>
              {id}
            </Typography>
            <Chip label={addr} className="device-addr" color="primary"/>
          </div>
          <Collapse in={this.state.hovering}>
            <Typography className="device-description" variant="body1" color="inherit">
              {device.description}
            </Typography>
          </Collapse>
        </div>
      </Paper>
    );
  }
}

export default DeviceTile;
