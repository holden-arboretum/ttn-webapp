export default {
  api: {
    url: 'http://34.232.30.167'
  },
  oauth: {
    redirect_uri: 'http://34.232.30.167/oauth/callback&response_type=code'
  },
  ttn: {
    base_uri: 'https://account.thethingsnetwork.org',
    oauth_uri: 'https://account.thethingsnetwork.org/users/authorize',
    client_id: 'bla',
    client_secret: 'secret'
  }
};
