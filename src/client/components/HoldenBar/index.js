import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';
import LinearProgress from '@material-ui/core/LinearProgress';

import 'styles/app-bar.sass';

class HoldenBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: ''
    };
  }

  changed(value) {
    this.props.onSearch(value);
    this.setState({ search: value });
  }

  render() {
    return (
      <AppBar position="static">
        <Toolbar>
          <Typography className="title" variant="h6" color="inherit" noWrap>
            Holden Arboretum
          </Typography>
          <div className="grow" />
          {this.props.showBack ? (
            <Button color="inherit" onClick={() => this.props.onBack()}>
              Back to devices
            </Button>
          ) : (
            <div className="search">
              <div className="search-icon">
                <SearchIcon />
              </div>
              <InputBase
                placeholder="Search Devices"
                onChange={event => this.changed(event.target.value)}
                classes={{
                  root: 'input-root',
                  input: 'input-input'
                }}
              />
            </div>
          )}
        </Toolbar>
        {this.props.loading && <LinearProgress color="secondary" />}
      </AppBar>
    )
  }
}

export default HoldenBar;
