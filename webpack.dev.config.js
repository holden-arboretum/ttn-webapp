const merge = require('webpack-merge');
const baseConfig = require('./webpack.base.config.js');
const path = require('path');

const config = {
  mode: 'development',
  devServer: {
    port: 8080,
    open: true,
    contentBase: path.join(__dirname, 'dist')
  },
  resolve: {
    alias: {
      config$: path.resolve(__dirname, 'config/development.js')
    }
  }
};

module.exports = merge(baseConfig, config);
