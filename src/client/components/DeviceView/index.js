import React from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Down from '@material-ui/icons/KeyboardArrowDown';
import Up from '@material-ui/icons/KeyboardArrowUp';
import Collapse from '@material-ui/core/Collapse';
import Divider from '@material-ui/core/Divider';
import Chip from '@material-ui/core/Chip';
import { DateTimePicker } from 'material-ui-pickers';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import UplinkTable from './uplink-table'
import {
  XYPlot,
  HorizontalGridLines,
  VerticalGridLines,
  XAxis,
  YAxis,
  LineSeries
} from 'react-vis';


import api from 'lib/api';
import { fixHexString } from 'lib/strings';
import { genData } from 'lib/table-helper';
import { lastSeen } from 'lib/device-list-helper';
import moment from 'moment';

import 'styles/device-view.sass';

function HexChip(props) {
  const { code, label } = props;
  return (
    <div className="hex-chip">
      <Typography variant="subtitle1" color="inherit" noWrap>
        {label}
      </Typography>
      <Chip
        label={fixHexString(code)}
        className="hex-code"
        color="primary"
      />
    </div>
  );
}

class DeviceView extends React.Component {
  constructor(props) {
    super(props);

    const start = moment()
      .month(new Date().getMonth() - 1)
      .hours(0)
      .minutes(0)
      .seconds(0);

    this.state = {
      uplinks: [],
      titleExpanded: false,
      start,
      end: moment(),
      value: 0
    };
  }

  componentDidMount() {
    this.getUplinks();
  }

  getUplinks() {
    const { start, end } = this.state;
    api.getUplinks(this.props.device, start, end)
      .then(uplinks => this.setState({ uplinks }))
  }

  dateChanged(date, start) {
    this.setState({ [start ? 'start': 'end']: date }, () => {
      this.getUplinks();
    });
  }

  render() {
    const { devId, description, devAddr, devEui, appEui } = this.props.device
    const { uplinks, titleExpanded, start, end, value} = this.state;
    const { temperature, humidity } = genData(uplinks);
    const data = value === 0 ? humidity : temperature;
    const axisTitle = value === 0 ? "Relative Humidity (%)" : "Temperature (°F)";

    return (
      <div className="main">
        <Paper
          className="container"
          onClick={() => this.setState({ titleExpanded: !titleExpanded})}
        >
          <div className="title">
            <Typography variant="h6" color="inherit" noWrap>
              {devId}
            </Typography>
            <div className="grow" />
            <Typography variant="body2">
              {lastSeen(this.props.device)}
            </Typography>
          </div>
          <Typography variant="body2" color="inherit">
            {description}
          </Typography>
          {titleExpanded && <Divider />}
          <Collapse in={titleExpanded}>
            <div className="more-info">
              <HexChip label="Device Address" code={devAddr} />
              <HexChip label="Device EUI" code={devEui} />
              <HexChip label="App EUI" code={appEui} />
            </div>
          </Collapse>
          <div className="flex">
            <div className="grow" />
            {titleExpanded ? <Up /> : <Down />}
          </div>
        </Paper>
        <Paper className="container">
          <div className="title">
            <Typography variant="h6" color="inherit" noWrap>
              Uplinks
            </Typography>
            <div className="grow" />
            <DateTimePicker value={start} onChange={date => this.dateChanged(date, true)} />
            <Typography className="date-seperator" variant="subtitle1" color="inherit" noWrap>
              to
            </Typography>
            <DateTimePicker
              value={end}
              onChange={date => this.dateChanged(date, false)}
            />
            <div className="grow" />
            <Chip label={uplinks.length} color="primary" />
          </div>
          <UplinkTable uplinks={uplinks} />
        </Paper>
        <Paper className="container">
          <div className="title">
            <Typography variant="h6" color="inherit" noWrap>
              Graph
            </Typography>
            <div className="grow" />
            <Tabs
              value={this.state.value}
              onChange={(event, value) => this.setState({ value })}
            >
              <Tab label="Humidity" />
              <Tab label="Temperature" />
            </Tabs>
          </div>

          {uplinks.length > 0 && (
            <XYPlot
              xType="time"
              width={1000}
              height={400}
            >
              <HorizontalGridLines />
              <VerticalGridLines />
              <XAxis title="Date" tickLabelAngle={-45} />
              <YAxis title={axisTitle} />
              <LineSeries data={data} />
            </XYPlot>
          )}
        </Paper>
      </div>
    )
  }
}

export default DeviceView;
