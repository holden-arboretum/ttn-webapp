import moment from 'moment';

export function calculateNewCols(width) {
  return Math.max(1, Math.floor(width / 380));
}

export function sliceDevices(col, cols, devices) {
  const filtered = [];

  for (let i = col; i < devices.length; i += cols) {
    filtered.push(devices[i]);
  }

  return filtered;
}

export function lastSeen(device) {
  const lastSeen = moment(device.last_seen);
  const days = moment().diff(lastSeen, 'days');
  if (days > 0) {
    return  `Last seen: ${days} ${days != 1 ? 'days' : 'day'} ago`;
  }
  const hours = moment().diff(lastSeen, 'hours');
  if (hours > 0) {
    return  `Last seen: ${hours} ${hours != 1 ? 'hours' : 'hour'} ago`;
  }
  const minutes = moment().diff(lastSeen, 'minutes');
  if (minutes > 0) {
    return  `Last seen: ${minutes} ${minutes != 1 ? 'minutes' : 'minute'} ago`;
  }
  const seconds = moment().diff(lastSeen, 'seconds');
  return  `Last seen: ${seconds} ${seconds != 1 ? 'seconds' : 'second'} ago`;
}
