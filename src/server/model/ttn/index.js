import { HandlerClient } from 'ttn';

const accessKey = 'ttn-account-v2.InORwAT0D0gnRMM4pkHOo6J_jAgotbMd8olUdr3ceqE';

async function getDevices() {
  const ttnHandler = new HandlerClient('rtu', accessKey);
  const client = await ttnHandler.open();
  const devices = await client.application().devices();
  return devices;
}

export default {
  getDevices
};
