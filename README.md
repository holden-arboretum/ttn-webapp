# ttn

This is a full stack web application written to display collected data from the holden arboretum lora sensor network

## Technologies

Before you dive into this you need to have an understanding of a few frameworks.

- React: A frontend javascript framework for creating views
- Express: A backend javascript framework written for nodeJS which I utilize for a REST API
- DynamoDb: An AWS service which allows for us to store uplink data
- AWS: You must know the basics of AWS such that you can spin up an EC2 instance to host the website
- NPM (node package manager): Manages all the packages and allows for commands to be run
- Webpack: Think of it as a packager for our web application

## Some Notes

There is no way I can dive into the complexity of this web app, so I will just say some of the important things:
- The web app is split into two sections, `client` and `server`. All frontend code lives in the `client` folder and all backend code lives in the `server` folder. When building for production, Webpack will compile all the javascript, css, and html in the `client` folder into a `static` folder such that the backend can serve up this static content.
- The data is grabbed from TTN via the `/uplink` endpoint. This is a `POST` endpoint and TTN will automatically make a `POST` request to the backend whenever there is a new uplink from a node. We get this data and store it in `DynamoDb` so we can retrieve it whenever.
- I was in the process of adding oauth support as there is no authentication currently and anyone can read the data. The endpoints and code is all there, I just never got around to testing it.

## Development and Deployment

Before you run any commands, make sure you run the command `npm install`. It will install all the necessary dependencies.

In order to do development, you can either run the command `npm run dev` or `npm run devw`. The first is for mac/linux and the secnd is for windows. This command will start both the client and the server in parallel. Connect to `localhost:8080` in order to access the website.

Deploying is a piece of cake. All you have to do is push your changes to master and then run `npm run deploy`. Do note that you will have to change the `deploy.sh` code slightly in order to put the correct IP address. Also, you must have the SSH keys for the remote instance defined in your ssh-config so that there is no password prompt. On EC2, you should have a `.pem` file which is your key. Make sure to have SSH key forwarding on so that the script is able to pull the repo on the remote. If it's a fresh instance, then you are going to have to first clone the repo before running the script.
