import config from 'config';

async function getDevices() {
    const response = await fetch(`${config.api.url}/v1/devices`);
    const json = await response.json();
    return json.devices;
}

async function getSeen(devices) {
  const params = {
    method: 'POST',
    body: JSON.stringify({
      devices: devices.map(device => device.devId)
    }),
    headers: {
      "Content-Type": "application/json"
    }
  };

  const response = await fetch(`${config.api.url}/v1/seen`, params);
  const json = await response.json();
  return json.uplinks;
}

async function getUplinks(device, start, end) {
  const params = {
    method: 'POST',
    body: JSON.stringify({
      devices: [device.devId],
      start: start.valueOf(),
      end: end.valueOf()
    }),
    headers: {
      "Content-Type": "application/json"
    }
  };

  const response = await fetch(`${config.api.url}/v1/uplinks`, params);
  const json = await response.json();
  return json.uplinks[device.devId];
}

async function getToken(code) {
  const { client_id, client_secret, base_uri } = config.ttn;
  const auth = btoa(`${client_id}:${client_secret}`);
  const params = {
    method: 'POST',
    body: JSON.stringify({
      grant_type: "authorization_code",
      code,
      redirect_uri: `${config.api.url}/oauth/token`
    }),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Basic ${auth}`
    }
  };

  fetch(`${base_uri}/users/token`, params);
}

export default {
  getDevices,
  getUplinks,
  getSeen
};
