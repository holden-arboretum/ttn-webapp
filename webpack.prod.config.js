const webpack = require('webpack');
const merge = require('webpack-merge');
const terserJS = require('terser-webpack-plugin');
const baseConfig = require('./webpack.base.config.js');
const path = require('path');

const config = {
  mode: 'production',
  output: {
    path: path.resolve(__dirname, 'src/server/public'),
    filename: 'bundle.js'
  },
  plugins: [
    new terserJS()
  ],
  resolve: {
    alias: {
      config$: path.resolve(__dirname, 'config/production.js')
    }
  }
};

module.exports = merge(baseConfig, config);
