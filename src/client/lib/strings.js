export function fixHexString(string) {
  string = string.toUpperCase();

  for (let i = 2; i < string.length; i+=3) {
    const first = string.substr(0, i);
    const last = string.substr(i, string.length);
    string = first + " " + last;
  }
  return string;
}
