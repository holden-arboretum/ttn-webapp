const defaultMessages = {
  ok: 'Success',
  created: 'Data has been generated on the server',
  badRequest: 'There was an issue with the request',
  notFound: 'This resource does not exist',
  forbidden: 'You are not allowed to access this',
  internalServerError: 'Something went wrong on the server'
};

const statusCodes = {
  ok: 200,
  created: 201,
  badRequest: 400,
  notFound: 404,
  forbidden: 403,
  internalServerError: 500
};

export default (req, res, next) => {
  Object.keys(statusCodes).forEach(key => {
    res[key] = (body, message = defaultMessages[key]) => {
      return res.status(statusCodes[key]).json({
        status: statusCodes[key],
        message,
        ...body
      });
    };
  });
  next();
};
