import React from 'react';
import DeviceTile from './device-tile';

import { calculateNewCols, sliceDevices } from 'lib/device-list-helper';

import 'styles/device-list.sass';

class DeviceList extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      cols: 4
    }
  }

  updateDimensions() {
    const cols = Math.min(4, calculateNewCols(window.innerWidth));
    if (cols != this.state.cols) {
      this.setState({ cols });
    }
  }

  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions.bind(this));
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions.bind(this));
  }

  renderCol(col, cols, devices) {
    const sliced = sliceDevices(col, cols, devices);
    const tiles = sliced.map((device, i) => {
      return (
        <DeviceTile
          device={device}
          key={`${device.devId}`}
          onClick={() => this.props.deviceSelected(device)}
        />
      );
    });

    return (
      <div className="device-list-col" key={`col-${col}`}>
        {tiles}
      </div>
    );
  }

  render() {
    const devices = this.props.devices;
    const cols = this.state.cols;
    const columns = [];

    for (let i = 0; i < cols; i++) {
      columns.push(this.renderCol(i, cols, devices));
    }

    return (
      <div className="device-container">
        {columns}
      </div>
    )
  }
}

export default DeviceList;
