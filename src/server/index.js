import express from 'express';
import http from 'http';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import middleware from './middleware'
import routes from './routes';
import oauth from './routes/oauth';
import path from 'path';
import cors from 'cors';

const app = express();
app.server = http.createServer(app);

app.use(express.json());
app.use(morgan('dev'));

app.use(middleware());
app.use(cors());
app.use('/v1', routes());
app.use('/', oauth());

if (process.env.NODE_ENV === 'PRODUCTION') {
  app.use('/', express.static(path.join(__dirname, 'public')));
}

const port = process.env.port || process.env.NODE_ENV === 'PRODUCTION' ? 8080 : 3000;
app.server.listen(port, () => {
  console.log(`Started server on port ${port}!`);
});
