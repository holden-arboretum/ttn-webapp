import { Router } from 'express';
import http from './http-response';

export default () => {
  const routes = Router();
  routes.use(http);
  return routes;
};
