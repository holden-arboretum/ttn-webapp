import { Router } from 'express';
import DynamoDb from '../model/DynamoDb';
import TTN from '../model/ttn';

export default () => {
  const api = Router();

  api.get('/', (req, res) => {
    res.ok({ version: '1.0.0' });
  });

  api.post('/uplink', (req, res) => {
    DynamoDb.insertRawUplink(req.body)
      .then(data => {
        res.ok();
      })
      .catch(error => {
        res.internalServerError({ error }, 'Failed to insert uplink into database');
      });
  });

  api.get('/devices', (req, res) => {
    TTN.getDevices()
      .then(devices => {
        res.ok({ devices });
      });
  });

  api.post('/uplinks', (req, res) => {
    const { devices, start, end } = req.body;
    DynamoDb.queryUplinks(devices, start, end)
      .then(uplinks => {
        res.ok({ uplinks });
      })
      .catch(error => {
        res.internalServerError({ error }, 'Failed to retrieve uplinks from database');
      });
  });

  api.post('/seen', (req, res) => {
    const devices = req.body.devices;
    DynamoDb.getLatest(devices)
      .then(uplinks => {
        res.ok({ uplinks });
      })
      .catch(error => {
        res.internalServerError({ error }, 'Failed to retrieve uplink from database');
      });
  });

  return api;
}
