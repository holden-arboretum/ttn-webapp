import moment from 'moment';

export function desc(a, b, orderBy) {
  if (orderBy === 'date') orderBy = 'time';
  if (orderBy === 'humidity' || orderBy === 'temperature') {
    b = b.data;
    a = a.data;
  }
  if (orderBy === 'frequency' || orderBy === 'data rate') {
    b = b.rf_data;
    a = a.rf_data;
  }

  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

export function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

export function getSorting(order, orderBy) {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

export const rows = [
  { id: 'date', numeric: false, disablePadding: true, label: 'Date' },
  { id: 'time', numeric: false, disablePadding: false, label: 'Time' },
  { id: 'humidity', numeric: true, disablePadding: false, label: 'Humidity (%)' },
  { id: 'temperature', numeric: true, disablePadding: false, label: 'Temperature (°F)' },
  { id: 'frequency', numeric: true, disablePadding: false, label: 'Frequency (Mhz)' },
  { id: 'dataRate', numeric: true, disablePadding: false, label: 'Data Rate' }
];

export function genData(uplinks) {
  const temperature = [];
  const humidity = [];

  uplinks.forEach(uplink => {
    const date = uplink.time;
    const hum = uplink.data.humidity;
    const temp = uplink.data.temperature;

    temperature.push({ x: date, y: temp });
    humidity.push({ x: date, y: hum });
  });

  return { temperature, humidity };
}

export function exportToCSV(uplinks) {
    let csv = generateCSV(uplinks);
    const filename = 'export.csv';

    csv = 'data:text/csv;charset=utf-8,' + csv;
    const data = encodeURI(csv);

    const link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', filename);
    link.click();

}

function generateCSV(uplinks) {
  const data = uplinks.map(uplink => {
    const Date = moment(uplink.time).format('MMMM Do YYYY');
    const Time = moment(uplink.time).format('hh:mm a');
    const Humidity = uplink.data.humidity;
    const Temperature = uplink.data.temperature;
    const Frequency = uplink.rf_data.frequency;
    const data_rate = uplink.rf_data.data_rate;

    return {
      Date,
      Time,
      Humidity,
      Temperature,
      Frequency,
      ['Data Rate']: data_rate
    };
  });

  let result;

  if (data === null) return null;

  const keys = Object.keys(data[0]);

  result = '';
  result += keys.join(',');
  result += '\n';

  data.forEach(item => {
    result += Object.values(item).join(',') + '\n';
  });

  return result;
}
