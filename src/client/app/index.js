import React from 'react';
import ReactDOM from 'react-dom'
import Root from '../components';

import '../../../node_modules/react-vis/dist/style.css';

const renderTo = document.createElement('div');

const font = document.createElement('link');
font.setAttribute('rel', 'stylesheet');
font.setAttribute('href', 'https://fonts.googleapis.com/css?family=Roboto:300,400,500');

const icons = document.createElement('link');
font.setAttribute('rel', 'stylesheet');
font.setAttribute('href', 'https://fonts.googleapis.com/icon?family=Material+Icons');

document.head.appendChild(font);
document.head.appendChild(icons);
document.body.appendChild(renderTo);

ReactDOM.render(<Root />, renderTo);
