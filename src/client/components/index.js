import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import HoldenBar from './HoldenBar';
import DeviceList from './DeviceList';
import DeviceView from './DeviceView'
import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import config from 'config';

import MomentUtils from '@date-io/moment';
import api from 'lib/api';
import { getCookie } from 'lib/cookie-manager'

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import teal from '@material-ui/core/colors/teal';
import blue from '@material-ui/core/colors/blue';
import 'styles/root.sass';

const theme = createMuiTheme({
  palette: {
    primary: teal,
    secondary: blue,
  },
});

class Root extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      devices: [],
      filtered: [],
      selected: {}
    };
  }

  componentDidMount() {
    const authCode = getCookie('authorization_code');
    const authToken = getCookie('token');

    if (authCode && authToken) {
      this.obtainDevices();
    } else if (authCode) {
      api.getToken(authCode)
        .then(() => this.obtainDevices())
    } else {
      window.location = `${config.ttn.oauth_uri}?client_id=${config.ttn.client_id}&redirect_uri=${config.oauth.redirect_uri}`
    }
  }

  obtainDevices() {
    api.getDevices()
      .then(devices => {
        api.getSeen(devices)
          .then(uplinks => {
            const newDevices = devices.map(device => {
              device.last_seen = uplinks[device.devId];
              return device;
            });
            this.setState({ devices: newDevices, filtered: newDevices });
          });
      });
  }

  filterDevices(text) {
    const filtered = this.state.devices.filter(device => {
      return device.devId.includes(text);
    });

    this.setState({ filtered });
  }

  render() {
    const selected = Object.keys(this.state.selected).length > 0;
    return (
      <MuiThemeProvider theme={theme}>
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <CssBaseline />
          <HoldenBar
            onSearch={text => this.filterDevices(text)}
            showBack={selected}
            onBack={() => this.setState({ selected: {} })}
            loading={this.state.devices.length == 0}
          />
          {selected ? (
            <DeviceView device={this.state.selected} />
          ) : (
            <DeviceList
              devices={this.state.filtered}
              deviceSelected={device => this.setState({ selected: device })}
            />
          )}
        </MuiPickersUtilsProvider>
      </MuiThemeProvider>
    )
  }
}

export default Root;
